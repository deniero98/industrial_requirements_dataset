# Industrial Requirements Dataset

This dataset includes a .json file with 233 industrial requirements. The sentences are labeled for a named entity recognition model. A word can either be labeled a Condition or an Instruction.

The .json file is a list of these objects:

```
{
    "sentence": "When the vehicle is involved in a detectable collision, the vehicle shall be brought to a standstill.",
    "entities": [
      {
        "start": 20,
        "end": 28,
        "type": "CON"
      },
      {
        "start": 77,
        "end": 84,
        "type": "INS"
      }
    ],
    "paper": {
      "title": "Proposal for a new UN Regulation on uniform provisions concerning the approval of vehicles with regards to Automated Lane Keeping Systems ECE/TRANS/WP.29/2020/81",
      "autor": "Economic Commission for Europe"
    }
}
```
